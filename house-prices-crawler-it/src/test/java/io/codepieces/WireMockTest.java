package io.codepieces;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

public class WireMockTest {

    private WireMockServer wireMockServer;

    @Before
    public void setUp() throws Exception {
        wireMockServer = new WireMockServer(wireMockConfig());
        wireMockServer.start();
    }

    @After
    public void tearDown() throws Exception {
        wireMockServer.stop();
        wireMockServer.shutdownServer();
    }

    @Test
    public void startWireMock() throws IOException, InterruptedException {
        Fix this
        stubFor(get(urlMatching("/bla?"))
                .withQueryParam("c", containing("2"))
//                .withQueryParam("radio", matching("1,1"))
//                .withQueryParam("page", matching("1"))
                .willReturn(
                aResponse()
                    .withStatus(200)
                    .withBody(IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream("s1/r1.json")))));

        while (true) {Thread.sleep(100);}
    }
}