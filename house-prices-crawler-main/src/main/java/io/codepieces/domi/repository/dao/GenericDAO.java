package io.codepieces.domi.repository.dao;

import java.util.List;

public interface GenericDAO<T, K> {
    void search(SearchCriteria<T> criteria);
    T getById(K key);
    void save(T t);
    void saveAll(List<T> t);
    void remove(T t);
}
