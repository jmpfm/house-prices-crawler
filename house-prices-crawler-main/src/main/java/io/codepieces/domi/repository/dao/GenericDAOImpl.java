package io.codepieces.domi.repository.dao;

import org.mongodb.morphia.dao.BasicDAO;

import java.util.List;

public class GenericDAOImpl<T, K> implements GenericDAO<T, K> {
    private final BasicDAO<T, K> mongoDAO;

    public GenericDAOImpl(BasicDAO <T, K> mongoDAO) {
        this.mongoDAO = mongoDAO;
    }

    @Override
    public void save(T t) {
        mongoDAO.save(t);
    }

    @Override
    public void search(SearchCriteria<T> criteria) {

    }

    @Override
    public T getById(K key) {
        return mongoDAO.get(key);
    }

    @Override
    public void saveAll(List<T> ts) {
        ts.forEach(mongoDAO::save);
    }

    @Override
    public void remove(T t) {
        mongoDAO.delete(t);
    }
}
