package io.codepieces.domi.repository.impl;

import io.codepieces.domi.model.HousePrice;
import io.codepieces.domi.repository.HousePricesRepository;
import io.codepieces.domi.repository.dao.GenericDAOImpl;
import org.mongodb.morphia.dao.BasicDAO;

public class HousePricesRepositoryImpl extends GenericDAOImpl<HousePrice, String> implements HousePricesRepository {
    public HousePricesRepositoryImpl(BasicDAO<HousePrice, String> mongoDAO) {
        super(mongoDAO);
    }
}
