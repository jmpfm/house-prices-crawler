package io.codepieces.domi.repository;

import io.codepieces.domi.model.HousePrice;
import io.codepieces.domi.repository.dao.GenericDAO;

public interface HousePricesRepository extends GenericDAO<HousePrice, String> {

}
