package io.codepieces.domi;

import com.typesafe.config.Config;
import io.codepieces.domi.configuration.ConfigurationLoader;
import io.codepieces.domi.factory.SchedulerFactory;
import io.codepieces.domi.factory.TaskFactory;
import io.codepieces.domi.scheduler.Scheduler;
import io.codepieces.domi.scheduler.Task;

import java.util.Arrays;
import java.util.List;

public class Application {

    private static final String PROFILE_ARG = "p:";
    private static final String EMPTY = "";
    private String profile;

    public static void main(String ... args) {
        new Application().parse(args).start();
    }

    public Application parse(String[] args) {
        String profileFromArgs = Arrays.stream(args)
                .filter(a -> a.startsWith(PROFILE_ARG))
                .findFirst()
                .map(this::toProfile)
                .orElse(EMPTY);
        if (!EMPTY.equals(profileFromArgs)) {
            profile = profileFromArgs;
        }
        return this;
    }

    private String toProfile(String profileFromArgs) {
        return profileFromArgs.substring(PROFILE_ARG.length(),profileFromArgs.length());
    }

    private void start() {
        Config config = ConfigurationLoader.load(profile);
        List<Task> task = TaskFactory.buildTasks(config);
        Scheduler scheduler = SchedulerFactory.build();
        if (scheduler != null) {
            task.forEach(scheduler::schedule);
            scheduler.start();
            while (true) {

            }
        }
    }

    public String getProfile() {
        return profile;
    }
}
