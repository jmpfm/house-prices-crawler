package io.codepieces.domi.scheduler.task;

import io.codepieces.domi.scheduler.Task;
import org.quartz.Job;
import org.quartz.JobDataMap;

public abstract class QuartzTask extends Task implements Job {
    protected JobDataMap jobData;

    public QuartzTask(JobDataMap jobDataMap) {
        jobData = jobDataMap;
    }

    public Class<? extends Job> getJob() {
        return this.getClass();
    }

    public JobDataMap getJobData() {
        return jobData;
    }
}
