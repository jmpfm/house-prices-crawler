package io.codepieces.domi.scheduler;

public abstract class Task {

    public enum TIME_FREQUENCY { WEEKLY, CRON }
    protected static final String WEEKLY_CRON = "0 0 0 ? * MON *";

    private String cron;
    private TIME_FREQUENCY frequency;

    public void setFrequency(String cron) {
        frequency = TIME_FREQUENCY.CRON;
        this.cron = cron;
    }

    public void setFrequency(TIME_FREQUENCY frequency) {
        this.frequency = frequency;
    }

    public TIME_FREQUENCY getFrequency() {
        return frequency;
    }

    public String getTimeFrequencyAsCron() {
        return TIME_FREQUENCY.WEEKLY.equals(frequency)
                ? WEEKLY_CRON
                : cron;
    }
}
