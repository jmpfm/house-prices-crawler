package io.codepieces.domi.scheduler.task;/*
 * Created by chema on 25/08/15.
 */

import io.codepieces.domi.scheduler.Task;

public class ScheduledTask {

    private final Task task;
    private final Task.TIME_FREQUENCY frequency;

    private ScheduledTask(Task task) {
        this.task = task;
        this.frequency = task.getFrequency();
    }

    public static ScheduledTask of(Task task) {
        return new ScheduledTask(task);
    }

    public Task getTask() {
        return task;
    }

    public Task.TIME_FREQUENCY getFrequency() {
        return frequency;
    }
}
