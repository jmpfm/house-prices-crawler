package io.codepieces.domi.scheduler.task;

import io.codepieces.domi.crawler.HousePricesCrawler;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.awt.geom.Point2D;

public class CrawlerTask extends QuartzTask {

    public CrawlerTask(JobDataMap jobDataMap) {
        super(jobDataMap);
    }

    //TODO redisign
    public CrawlerTask() {
        super(null);
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        HousePricesCrawler crawler = (HousePricesCrawler) context.getMergedJobDataMap().get("crawler");
        Point2D center = (Point2D) context.getMergedJobDataMap().get("center");
        Point2D radius = (Point2D) context.getMergedJobDataMap().get("radius");
        crawlLocation(crawler, center, radius);
    }

    private void crawlLocation(HousePricesCrawler crawler, Point2D center, Point2D radius) {
        try {
            crawler.storePricesForLocation(center, radius);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
