package io.codepieces.domi.scheduler.adapter;

import io.codepieces.domi.scheduler.Task;
import io.codepieces.domi.scheduler.task.ScheduledTask;

public interface SchedulerAdapter {
    ScheduledTask addTask(Task task);

    void start();
}
