package io.codepieces.domi.scheduler.adapter.impl;

import io.codepieces.domi.scheduler.Task;
import io.codepieces.domi.scheduler.adapter.SchedulerAdapter;
import io.codepieces.domi.scheduler.task.QuartzTask;
import io.codepieces.domi.scheduler.task.ScheduledTask;
import org.quartz.*;

public class QuartzSchedulerAdapterImpl implements SchedulerAdapter {
    private final Scheduler scheduler;

    public QuartzSchedulerAdapterImpl(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    @Override
    public ScheduledTask addTask(Task task) {
        if (!(task instanceof QuartzTask))
            throw new IllegalArgumentException("task must be QuartzTask");

        QuartzTask quartzTask = (QuartzTask) task;
        try {
            JobDetail job = buildJobDetailFrom(quartzTask);
            Trigger trigger = buildTriggerFrom(quartzTask);
            scheduler.scheduleJob(job, trigger);

            return ScheduledTask.of(task);
        } catch (SchedulerException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void start() {
        try {
            scheduler.start();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    private Trigger buildTriggerFrom(QuartzTask task) {
        return TriggerBuilder.newTrigger()
                             .withIdentity("TriggerFor" + task.getClass().getCanonicalName())
                             .withSchedule(
                                     CronScheduleBuilder.cronSchedule(task.getTimeFrequencyAsCron()))
                             .build();
    }

    private JobDetail buildJobDetailFrom(QuartzTask quartzTask) {
        return JobBuilder.newJob(quartzTask.getJob())
                         .usingJobData(quartzTask.getJobData())
                         .build();
    }
}
