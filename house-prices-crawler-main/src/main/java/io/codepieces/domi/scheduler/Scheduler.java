package io.codepieces.domi.scheduler;

import io.codepieces.domi.scheduler.adapter.SchedulerAdapter;
import io.codepieces.domi.scheduler.task.ScheduledTask;

public class Scheduler {

    private final SchedulerAdapter schedulerAdapter;

    public Scheduler(SchedulerAdapter schedulerAdapter) {
        this.schedulerAdapter = schedulerAdapter;
    }

    public ScheduledTask schedule(Task task) {
        return scheduleTask(task);
    }

    private ScheduledTask scheduleTask(Task task) {
        return schedulerAdapter.addTask(task);
    }

    public void start() {
        schedulerAdapter.start();
    }
}
