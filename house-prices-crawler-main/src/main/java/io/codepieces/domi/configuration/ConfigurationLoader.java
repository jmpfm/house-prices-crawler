package io.codepieces.domi.configuration;/*
 * Created by chema on 26/08/15.
 */

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class ConfigurationLoader {
    public static Config load(String profile) {
        Config config = ConfigFactory.load("application");
        return config.getConfig("domi.profiles." + profile);
    }
}
