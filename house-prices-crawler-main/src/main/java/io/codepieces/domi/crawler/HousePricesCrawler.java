package io.codepieces.domi.crawler;

import java.awt.geom.Point2D;

public interface HousePricesCrawler {
    void storePricesForLocation(Point2D center, Point2D radius) throws Exception;
}
