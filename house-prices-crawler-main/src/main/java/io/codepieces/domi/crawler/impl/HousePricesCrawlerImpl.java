package io.codepieces.domi.crawler.impl;

import io.codepieces.domi.api.client.HousePricesClient;
import io.codepieces.domi.crawler.HousePricesCrawler;
import io.codepieces.domi.repository.HousePricesRepository;

import java.awt.geom.Point2D;

public class HousePricesCrawlerImpl implements HousePricesCrawler {

    private final HousePricesClient housingPricesClient;
    private final HousePricesRepository housePricesRepository;

    public HousePricesCrawlerImpl(HousePricesClient housingPricesClient, HousePricesRepository housePricesRepository) {
        this.housingPricesClient = housingPricesClient;
        this.housePricesRepository = housePricesRepository;
    }

    @Override
    public void storePricesForLocation(Point2D center, Point2D radius) throws Exception {
        housingPricesClient.getPricesFor(center, radius)
                           .thenAccept(housePricesRepository::saveAll);
    }
}
