package io.codepieces.domi.api.client.base;

import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.Response;

import java.util.concurrent.CompletableFuture;

public class AsyncHttpClientImpl implements AsyncHttpClient {

    private final com.ning.http.client.AsyncHttpClient asyncHttpClient;
    private String base;

    public AsyncHttpClientImpl(String base) {
        this.base = base;
        asyncHttpClient = new com.ning.http.client.AsyncHttpClient();
    }

    @Override
    public CompletableFuture<Response> doGetTo(String url){
        final CompletableFuture<Response> toBeCompleted = new CompletableFuture<>();
        asyncHttpClient.prepareGet(base + url).execute(new AsyncCompletionHandler<Response>() {
            @Override
            public Response onCompleted(Response response) throws Exception {
                toBeCompleted.complete(response);
                return response;
            }
            @Override
            public void onThrowable(Throwable t){
                toBeCompleted.completeExceptionally(t);
            }
        });
        return toBeCompleted;
    }
}
