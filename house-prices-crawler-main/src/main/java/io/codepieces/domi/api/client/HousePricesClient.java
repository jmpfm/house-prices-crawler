package io.codepieces.domi.api.client;

import io.codepieces.domi.model.HousePrice;

import java.awt.geom.Point2D;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface HousePricesClient {
    CompletableFuture<List<HousePrice>> getPricesFor(Point2D center, Point2D radius) throws Exception;
}
