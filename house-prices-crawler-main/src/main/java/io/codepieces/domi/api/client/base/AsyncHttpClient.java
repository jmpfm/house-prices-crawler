package io.codepieces.domi.api.client.base;

import com.ning.http.client.Response;

import java.util.concurrent.CompletableFuture;

public interface AsyncHttpClient {
    CompletableFuture<Response> doGetTo(String url);
}
