package io.codepieces.domi.api.transformer;

public interface ResponseTransformer<T> {

    T transform(String responseBody);
}
