package io.codepieces.domi.api.response;

import io.codepieces.domi.model.HousePrice;

import java.util.List;

public class S1Response {

    private int itemsPerPage;
    private int total;
    private int totalPages;
    private int upperRangePosition;
    private List<HousePrice> elementList;
    private String query;

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(int itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getUpperRangePosition() {
        return upperRangePosition;
    }

    public void setUpperRangePosition(int upperRangePosition) {
        this.upperRangePosition = upperRangePosition;
    }

    public List<HousePrice> getElementList() {
        return elementList;
    }

    public void setElementList(List<HousePrice> elementList) {
        this.elementList = elementList;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
