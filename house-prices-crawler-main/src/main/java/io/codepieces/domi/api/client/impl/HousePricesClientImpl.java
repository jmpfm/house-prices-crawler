package io.codepieces.domi.api.client.impl;

import com.ning.http.client.Response;
import com.typesafe.config.Config;
import io.codepieces.domi.api.client.HousePricesClient;
import io.codepieces.domi.api.client.base.AsyncHttpClient;
import io.codepieces.domi.api.response.S1Response;
import io.codepieces.domi.api.transformer.ResponseTransformer;
import io.codepieces.domi.model.HousePrice;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

public class HousePricesClientImpl implements HousePricesClient{

    private static final String S1_URL_KEY = "s1.url";
    private static final String DEFINED_DELAY = "s1.delay";

    private final AsyncHttpClient asyncClient;
    private final ResponseTransformer<S1Response> responseTransformer;
    private final String s1Endpoint;
    private final long definedDelay;

    public HousePricesClientImpl(AsyncHttpClient asyncClient,
                                 ResponseTransformer<S1Response> responseTransformer,
                                 Config clientConfig) {
        this.asyncClient = asyncClient;
        this.responseTransformer = responseTransformer;
        this.s1Endpoint = clientConfig.getString(S1_URL_KEY);
        this.definedDelay = clientConfig.getLong(DEFINED_DELAY);
    }

    @Override
    public CompletableFuture<List<HousePrice>> getPricesFor(Point2D center, Point2D radius) throws Exception {
        CompletableFuture<S1Response> futureResponse =
                performGetToS1(center, radius, 1);

        CompletableFuture<List<HousePrice>> housePrices = futureResponse.thenApply(S1Response::getElementList);

        //Blocking
        S1Response response = futureResponse.get();

        for (int i = 2; i <= response.getTotalPages(); i++) {
            performGetToS1(center, radius, i)
                .thenApply(addHousePriceToPriceList(housePrices))
                .thenAccept(this::applyDelay);
        }
        return housePrices;
    }

    private Function<S1Response, CompletableFuture<Void>> addHousePriceToPriceList(CompletableFuture<List<HousePrice>> housePrices) {
        return r -> housePrices.thenAccept(hp -> hp.addAll(r.getElementList()));
    }

    private CompletableFuture<S1Response> performGetToS1(Point2D center, Point2D radius, int i) {
        return asyncClient.doGetTo(String.format(s1Endpoint,
                center.getX(), center.getY(),
                radius.getX(), radius.getY(), i))
            .thenApply(this::getResponseBodyFromResponse)
            .thenApply(responseTransformer::transform);
    }

    private String getResponseBodyFromResponse(Response response) {
        try {
            return response.getResponseBody();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void applyDelay(CompletableFuture<Void> ignoredArgument) {
        try {
            Thread.sleep(definedDelay);
        } catch (InterruptedException ignored) { }
    }
}
