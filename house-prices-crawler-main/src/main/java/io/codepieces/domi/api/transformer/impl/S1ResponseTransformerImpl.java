package io.codepieces.domi.api.transformer.impl;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import io.codepieces.domi.api.response.S1Response;
import io.codepieces.domi.api.transformer.ResponseTransformer;
import io.codepieces.domi.model.HousePrice;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

public class S1ResponseTransformerImpl implements ResponseTransformer<S1Response> {

    private final Gson gson;

    public S1ResponseTransformerImpl() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(S1Response.class, new S1ResponseAdapter());
        gson = builder.create();
    }

    @Override
    public S1Response transform(String responseBody) {
        return gson.fromJson(responseBody, S1Response.class);
    }

    private class S1ResponseAdapter implements JsonDeserializer<S1Response> {

        @Override
        public S1Response deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            JsonArray array = jsonElement.getAsJsonArray();
            String query = array.get(0).getAsString();
            S1Response s1Response = new S1Response();
            s1Response.setQuery(query);
            JsonObject innerObject = array.get(1).getAsJsonObject();
            List<HousePrice> elementList = jsonDeserializationContext.deserialize(innerObject.get("elementList"), new TypeToken<List<HousePrice>>(){}.getType());
            elementList.forEach( e -> e.setCreated(new Date()));
            s1Response.setElementList(elementList);
            s1Response.setTotalPages(innerObject.get("totalPages").getAsInt());
            s1Response.setItemsPerPage(innerObject.get("itemsPerPage").getAsInt());
            s1Response.setTotal(innerObject.get("total").getAsInt());
            s1Response.setUpperRangePosition(innerObject.get("upperRangePosition").getAsInt());
            return s1Response;
        }
    }
}
