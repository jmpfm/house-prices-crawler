package io.codepieces.domi.factory;/*
 * Created by chema on 26/08/15.
 */

import io.codepieces.domi.scheduler.Scheduler;
import io.codepieces.domi.scheduler.adapter.impl.QuartzSchedulerAdapterImpl;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

public class SchedulerFactory {
    public static Scheduler build() {
        try {
            return new Scheduler(new QuartzSchedulerAdapterImpl(new StdSchedulerFactory().getScheduler()));
        } catch (SchedulerException e) {
            e.printStackTrace();
            return null;
        }
    }
}
