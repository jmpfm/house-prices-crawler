package io.codepieces.domi.factory;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;
import com.typesafe.config.Config;
import io.codepieces.domi.model.HousePrice;
import io.codepieces.domi.repository.HousePricesRepository;
import io.codepieces.domi.repository.impl.HousePricesRepositoryImpl;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.dao.BasicDAO;

public class HousePricesRepositoryFactory {
    public static HousePricesRepository build(Config config) {
        String host = config.getString("mongo.host");
        int port = config.getInt("mongo.port");
        String dataBase = config.getString("mongo.db");

        ServerAddress addr = new ServerAddress(host, port);
        MongoClientOptions  options = MongoClientOptions.builder().build();
        MongoClient mongo = new MongoClient(addr, options);
        Morphia morphia = new Morphia();
        morphia.map(HousePrice.class);

        Datastore datastore = morphia.createDatastore(mongo, dataBase);
        BasicDAO<HousePrice, String> basicDAO = new BasicDAO<>(HousePrice.class, datastore);
        return new HousePricesRepositoryImpl(basicDAO);
    }
}
