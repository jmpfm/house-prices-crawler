package io.codepieces.domi.factory;/*
 * Created by chema on 29/08/15.
 */

import com.typesafe.config.Config;
import io.codepieces.domi.api.client.HousePricesClient;
import io.codepieces.domi.api.client.base.AsyncHttpClientImpl;
import io.codepieces.domi.api.client.impl.HousePricesClientImpl;
import io.codepieces.domi.api.response.S1Response;
import io.codepieces.domi.api.transformer.ResponseTransformer;
import io.codepieces.domi.api.transformer.impl.S1ResponseTransformerImpl;

public class HousePricesClientFactory {

    public static HousePricesClient build(Config config) {
        AsyncHttpClientImpl asyncClient = new AsyncHttpClientImpl(config.getString("s1.endpoint"));
        ResponseTransformer<S1Response> responseTransformer = new S1ResponseTransformerImpl();
        return new HousePricesClientImpl(asyncClient, responseTransformer, config);
    }
}
