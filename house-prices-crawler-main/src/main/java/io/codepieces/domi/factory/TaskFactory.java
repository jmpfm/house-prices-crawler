package io.codepieces.domi.factory;

import com.google.common.collect.ImmutableList;
import com.typesafe.config.Config;
import io.codepieces.domi.api.client.HousePricesClient;
import io.codepieces.domi.crawler.HousePricesCrawler;
import io.codepieces.domi.crawler.impl.HousePricesCrawlerImpl;
import io.codepieces.domi.repository.HousePricesRepository;
import io.codepieces.domi.scheduler.Task;
import io.codepieces.domi.scheduler.task.CrawlerTask;
import org.quartz.JobDataMap;

import java.awt.geom.Point2D;
import java.util.List;

public class TaskFactory {
    public static List<Task> buildTasks(Config config) {
        HousePricesClient client = HousePricesClientFactory.build(config);
        HousePricesRepository housePricesRepository = HousePricesRepositoryFactory.build(config);
        HousePricesCrawler crawler = new HousePricesCrawlerImpl(client, housePricesRepository);
        JobDataMap jobDataMap = createJobDataMap(config, crawler);
        Task crawlerTask = new CrawlerTask(jobDataMap);
        crawlerTask.setFrequency(config.getString("task.crawler.schedule"));
        return ImmutableList.of(crawlerTask);
    }

    private static JobDataMap createJobDataMap(Config config, HousePricesCrawler crawler) {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("crawler", crawler);
        jobDataMap.put("center",
                new Point2D.Double(
                        config.getDouble("crawler.center.x"),
                        config.getDouble("crawler.center.y")));
        jobDataMap.put("radius",
                new Point2D.Double(
                        config.getDouble("crawler.radius.x"),
                        config.getDouble("crawler.radius.y")));
        return jobDataMap;
    }
}
