package io.codepieces.domi.scheduler;

import io.codepieces.domi.scheduler.adapter.SchedulerAdapter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class SchedulerTest {

    private Scheduler scheduler;

    @Mock
    SchedulerAdapter schedulerAdapter;

    @Before
    public void setUp() throws Exception {
        scheduler = new Scheduler(schedulerAdapter);
    }

    @Test
    public void proxiesScheduleTaskToAdapter() {
        Task task = Mockito.mock(Task.class);
        scheduler.schedule(task);

        verify(schedulerAdapter).addTask(task);
    }

    @Test
    public void proxiesStartToAdapter() {
        scheduler.start();
        verify(schedulerAdapter).start();
    }
}
