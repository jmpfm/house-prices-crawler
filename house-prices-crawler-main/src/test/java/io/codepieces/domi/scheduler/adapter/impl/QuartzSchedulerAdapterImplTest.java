package io.codepieces.domi.scheduler.adapter.impl;

import io.codepieces.domi.scheduler.adapter.SchedulerAdapter;
import io.codepieces.domi.scheduler.task.QuartzTask;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.quartz.*;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class QuartzSchedulerAdapterImplTest {

    @Mock private Scheduler scheduler;
    private SchedulerAdapter quarztSchedulerAdapter;

    @Before
    public void setUp() throws Exception {
        quarztSchedulerAdapter = new QuartzSchedulerAdapterImpl(scheduler);
    }

    @Test
    public void schedulerAddsTask() throws SchedulerException {

        Class<MockJob> mockJobClass = MockJob.class;
        QuartzTask task = Mockito.mock(QuartzTask.class);
        when(task.getJobData()).thenReturn(Mockito.mock(JobDataMap.class));
        when(task.getJob()).thenReturn((Class) mockJobClass);
        when(task.getTimeFrequencyAsCron()).thenReturn("0/5 * * * * ?");
        quarztSchedulerAdapter.addTask(task);

        verify(scheduler).scheduleJob(any(JobDetail.class), any(Trigger.class));
    }
}

abstract class MockJob implements Job{}