package io.codepieces.domi.repository.impl;

import com.github.fakemongo.junit.FongoRule;
import com.mongodb.MongoClient;
import io.codepieces.domi.model.HousePrice;
import org.bson.Document;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.dao.BasicDAO;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class HousePricesRepositoryImplTest {

    private static final String TEST_DB = "TEST_DB";
    private HousePricesRepositoryImpl housePricesRepository;

    @Rule
    public FongoRule fongoRule = new FongoRule();

    @Before
    public void setUp() throws Exception {
        MongoClient mongo = fongoRule.getMongoClient();
        mongo.getDatabase(TEST_DB);
        Morphia morphia = new Morphia();
        morphia.map(HousePrice.class);
        Datastore datastore = morphia.createDatastore(mongo, TEST_DB);
        BasicDAO<HousePrice, String> basicDAO = new BasicDAO<>(HousePrice.class, datastore);
        housePricesRepository = new HousePricesRepositoryImpl(basicDAO);
    }

    //TODO this test makes me sad :(
    @Test
    public void saveHousePrice() {
        HousePrice housePrice = new HousePrice();
        housePrice.setPrice(450000);
        housePrice.setAddress("best possible place");
        housePricesRepository.save(housePrice);
        assertNotNull(housePrice.getId());
        checkCanLoadObject(housePrice.getId());
    }

    private void checkCanLoadObject(String id) {
        Document storedDocument = fongoRule.getMongoClient()
            .getDatabase(TEST_DB).getCollection("HousePrice").find().first();
        System.out.println(storedDocument.get("_id").getClass());
        assertThat(storedDocument.get("_id").toString(), is(id));
    }
}