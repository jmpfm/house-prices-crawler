package io.codepieces.domi.crawler.impl;

import io.codepieces.domi.api.client.HousePricesClient;
import io.codepieces.domi.crawler.HousePricesCrawler;
import io.codepieces.domi.model.HousePrice;
import io.codepieces.domi.repository.HousePricesRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class HousePricesCrawlerImplTest {

    private HousePricesCrawler housePricesCrawler;

    @Mock
    private HousePricesClient housePricesClient;

    @Mock
    private HousePricesRepository housePricesRepository;

    @Before
    public void setup() {
        housePricesCrawler = new HousePricesCrawlerImpl(housePricesClient, housePricesRepository);
    }
    
    @Test
    public void requestsPricesPerCoordinates() throws Exception {
        Point2D center = new Point2D.Float(7, 2);
        Point2D radius = new Point2D.Float(4, 7);
        given(housePricesClient.getPricesFor(center, radius)).willReturn(CompletableFuture.completedFuture(new ArrayList<>()));

        housePricesCrawler.storePricesForLocation(center, radius);

        verify(housePricesClient).getPricesFor(center, radius);
    }

    @Test
    public void storesSuccessfulResult() throws Exception {
        Point2D center = new Point2D.Float(1, 3);
        Point2D radius = new Point2D.Float(5, 1);
        List<HousePrice> housePrices = new ArrayList<>();

        given(housePricesClient.getPricesFor(center, radius)).willReturn(CompletableFuture.completedFuture(housePrices));

        housePricesCrawler.storePricesForLocation(center, radius);

        verify(housePricesRepository).saveAll(housePrices);
    }
}