package io.codepieces.domi;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.assertj.core.api.StrictAssertions.assertThat;


public class ApplicationTest {

    private Application application;

    @Before
    public void setUp() {
        application = new Application();
    }

    @Test
    public void assignsProfileIfPresent() {
        String args = "p:test";
        application.parse(args.split(" "));

        assertThat(application.getProfile()).isEqualTo("test");
    }

    @Test
    public void profileNullIfNotPresent() {
        String args = "foo:bar";
        application.parse(args.split(" "));

        assertThat(application.getProfile()).isNull();
    }

    @Test @Ignore
    public void startsApplication() {
        Application.main("p:test".split(" "));
    }
}