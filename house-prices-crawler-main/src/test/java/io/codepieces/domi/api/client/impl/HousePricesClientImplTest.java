package io.codepieces.domi.api.client.impl;

import com.google.common.collect.Lists;
import com.ning.http.client.Response;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValueFactory;
import io.codepieces.domi.api.client.HousePricesClient;
import io.codepieces.domi.api.client.base.AsyncHttpClient;
import io.codepieces.domi.api.response.S1Response;
import io.codepieces.domi.api.transformer.ResponseTransformer;
import io.codepieces.domi.model.HousePrice;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class HousePricesClientImplTest {

    private static final String URL_FORMAT = "x1:%.3fy1:%3fx2:%.3fy2:%3f-%d";
    private HousePricesClient housePricesClient;

    @Mock
    private AsyncHttpClient asyncClient;

    @Mock
    private ResponseTransformer<S1Response> iDResponseTransformer;

    @Before
    public void setUp() throws Exception {
        housePricesClient = new HousePricesClientImpl(asyncClient, iDResponseTransformer, withMockedConfig());
    }

    private Config withMockedConfig() {
        return ConfigFactory.empty()
                            .withValue("s1.url", ConfigValueFactory.fromAnyRef(URL_FORMAT))
                            .withValue("s1.delay", ConfigValueFactory.fromAnyRef(1));
    }

    @Test
    public void makesHttpRequest() throws Exception {
        Point2D center = new Point2D.Float(2, 3);
        Point2D radius = new Point2D.Float(5, 3);
        String expectedUrl = buildUrlFromPointsAndPage(center, radius, 1);
        Response response = mock(Response.class);
        given(asyncClient.doGetTo(expectedUrl)).willReturn(CompletableFuture.completedFuture(response));
        given(iDResponseTransformer.transform(anyString())).willReturn(new S1Response());

        housePricesClient.getPricesFor(center, radius);

        verify(asyncClient).doGetTo(expectedUrl);
    }

    @Test
    public void transformsResponse() throws Exception {
        Point2D center = new Point2D.Float(9, 6);
        Point2D radius = new Point2D.Float(5, 6);
        Response mockedResponse = mock(Response.class);
        given(asyncClient.doGetTo(anyString())).willReturn(CompletableFuture.completedFuture(mockedResponse));
        given(iDResponseTransformer.transform(anyString())).willReturn(new S1Response());

        housePricesClient.getPricesFor(center, radius);

        verify(iDResponseTransformer).transform(anyString());
    }
    
    @Test
    public void performsMultiplesRequest() throws Exception {
        Point2D center = new Point2D.Float(7, 9);
        Point2D radius = new Point2D.Float(1, 2);
        List<S1Response> mockedResponse = buildMockedResponseList();
        String firstUrl = buildUrlFromPointsAndPage(center, radius, 1);
        String secondUrl = buildUrlFromPointsAndPage(center, radius, 2);
        String thirdUrl = buildUrlFromPointsAndPage(center, radius, 3);
        String forthUrl = buildUrlFromPointsAndPage(center, radius, 4);

        given(asyncClient.doGetTo(anyString())).willReturn(CompletableFuture.completedFuture(mock(Response.class)));
        given (iDResponseTransformer.transform(anyString()))
            .willReturn(mockedResponse.get(0))
            .willReturn(mockedResponse.get(1))
            .willReturn(mockedResponse.get(2))
            .willReturn(mockedResponse.get(3));

        CompletableFuture<List<HousePrice>> prices = housePricesClient.getPricesFor(center, radius);
        List<HousePrice> housePrices = prices.get();

        verify(asyncClient).doGetTo(firstUrl);
        verify(asyncClient).doGetTo(secondUrl);
        verify(asyncClient).doGetTo(thirdUrl);
        verify(asyncClient).doGetTo(forthUrl);

        assertThat(housePrices.size(), is(6));
    }

    private String buildUrlFromPointsAndPage(Point2D center, Point2D radius, int page) {
        return String.format(URL_FORMAT, center.getX(), center.getY(), radius.getX(), radius.getY(), page);
    }

    private List<S1Response> buildMockedResponseList() {
        S1Response response = new S1Response();
        response.setTotalPages(4);
        response.setElementList(Lists.newArrayList(new HousePrice(), new HousePrice(), new HousePrice()));

        S1Response response2 = new S1Response();
        response2.setTotalPages(4);
        response2.setElementList(Lists.newArrayList());

        S1Response response3 = new S1Response();
        response3.setTotalPages(4);
        response3.setElementList(Lists.newArrayList(new HousePrice(), new HousePrice()));

        S1Response response4 = new S1Response();
        response4.setTotalPages(4);
        response4.setElementList(Lists.newArrayList(new HousePrice()));

        return  Lists.newArrayList(response, response2, response3, response4);
    }

    @Ignore //Exception not propagated ¿¿??
    @Test(expected = RuntimeException.class)
    public void propagatesIOExceptionAsRuntimeException() throws Exception {
        Point2D center = new Point2D.Float(9, 6);
        Point2D radius = new Point2D.Float(5, 6);
        Response mockedResponse = mock(Response.class);
        Response exceptionResponse = mock(Response.class);
        S1Response response = new S1Response();
        response.setTotalPages(4);

        given(exceptionResponse.getResponseBody()).willThrow(new IOException());
        given(asyncClient.doGetTo(anyString()))
                .willReturn(CompletableFuture.completedFuture(mockedResponse))
                .willReturn(CompletableFuture.completedFuture(exceptionResponse));
        given(iDResponseTransformer.transform(anyString())).willReturn(response);

        Future future = housePricesClient.getPricesFor(center, radius);
        future.get();
    }
}