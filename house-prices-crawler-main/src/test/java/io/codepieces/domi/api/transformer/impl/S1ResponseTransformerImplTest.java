package io.codepieces.domi.api.transformer.impl;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import io.codepieces.domi.api.response.S1Response;
import io.codepieces.domi.api.transformer.ResponseTransformer;
import io.codepieces.domi.model.HousePrice;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class S1ResponseTransformerImplTest {

    private ResponseTransformer<S1Response> s1ResponseTransformer;
    private String content;

    @Before
    public void setUp() throws Exception {
        URL url = Resources.getResource("responses/s1/s1ResponsePage1.json");
        content = Resources.toString(url, Charsets.UTF_8);

        s1ResponseTransformer = new S1ResponseTransformerImpl();
    }

    @Test
    public void transformsQuery() throws IOException {
        S1Response response = s1ResponseTransformer.transform(content);
        assertEquals("Pisos, Áticos, Estudios, " +
                "Dúplex, Chalets, De menos de 1200 euros , " +
                "De todos los tamaños, Con  dormitorios o más, En madrid ",
                response.getQuery());
    }
    
    @Test
    public void loadsTotalPages() throws IOException {
        S1Response response = s1ResponseTransformer.transform(content);

        assertThat(response.getTotalPages(), is(253));
    }

    @Test
    public void loadsItemsPage() throws IOException {
        S1Response response = s1ResponseTransformer.transform(content);

        assertThat (response.getItemsPerPage(), is(20));
    }

    @Test
    public void loadsTotal() {
        S1Response response = s1ResponseTransformer.transform(content);

        assertThat(response.getTotal(), is(5055));
    }
    
    @Test
    public void loadsUpperRangePosition() {
        S1Response response = s1ResponseTransformer.transform(content);

        assertThat(response.getUpperRangePosition(), is(20));
    }

    @Test
    public void loadsHousePrice() {
        S1Response response = s1ResponseTransformer.transform(content);
        HousePrice housePrice = response.getElementList().get(0);

        assertThat(housePrice.getAddress(), is("marques de leganes"));
        assertThat(housePrice.getBathrooms(), is(0));
        assertThat(housePrice.getCountry(), is("es"));
        assertThat(housePrice.getDistrict(), is("centro"));
        assertThat(housePrice.getFloor(), is(""));
        assertThat(housePrice.getLatitude(), is(40.42017F));
        assertThat(housePrice.getLongitude(), is(-3.7056978F));
        assertThat(housePrice.getMunipality(), is("madrid"));
        assertThat(housePrice.getNeighborhood(), is("malasaña-universidad"));
        assertThat(housePrice.getOperation(), is("A"));
        assertThat(housePrice.getPrice(), is(950));
        assertThat(housePrice.getPropertyCode(), is("28975482"));
        assertThat(housePrice.getPropertyType(), is("Dúplex"));
        assertThat(housePrice.getPropertyTypeCode(), is("VD"));
        assertThat(housePrice.getProvince(), is("madrid"));
        assertThat(housePrice.getRegion(), is(""));
        assertThat(housePrice.getRooms(), is(1));
        assertThat(housePrice.getSize(), is(85));
        assertThat(housePrice.getSubregion(), is(""));
        assertThat(housePrice.getUrl(), is("www.s1.com/28975482"));
        assertThat(housePrice.getUserCode(), is("113414672"));
    }
}